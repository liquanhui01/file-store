package repo

import (
	"context"
	"errors"
	md "file-store/models"
)

// CreateFile: 创建文件
func CreateFile(ctx context.Context, file *md.File) (id int64, err error) {
	fi := &md.File{
		FileSha1:    file.FileSha1,
		FileName:    file.FileName,
		FileSize:    file.FileSize,
		FileAddress: file.FileAddress,
		Status:      0,
		CreatedAt:   file.CreatedAt,
		UpdatedAt:   file.UpdatedAt,
	}
	return fi.ID, md.DB.Create(fi).Error
}

// FindFileByFilehash: 根据filesha1查找指定的文件
func FindFileByFileSha1(ctx context.Context, fileSha1 string) (
	file *md.File, err error) {
	return file, md.DB.WithContext(ctx).Where("file_sha1 = ?", fileSha1).
		First(&file).Error
}

// FindFileQueryByLimit: 查询最新的几条文件数据
func FindFileQueryByLimit(ctx context.Context, limit int64) (files []*md.File, err error) {
	md.DB.Raw("SELECT * FROM files ORDER BY id DESC LIMIT ?", limit).Scan(&files)
	return files, nil
}

// DeleteFileByFilehash: 根据文件filesha1删除文件
func DeleteFileByFilehash(ctx context.Context, filesha1 string) error {
	return md.DB.WithContext(ctx).Where("file_sha1 = ?", filesha1).Delete(&md.File{}).Error
}

// UpdateFileNameByFilehash: 根据filesha1更新文件的文件名(重命名)
func UpdateFileNameByFilehash(ctx context.Context,
	filesha1, fileName string) error {
	return md.DB.WithContext(ctx).Model(&md.File{}).Where("file_sha1 = ?", filesha1).
		Update("file_name", fileName).Error
}

// FindFileByName: 根据fileName查询文件是否已存在
func FindFileByName(ctx context.Context, fileName string) (file *md.File, err error) {
	row := md.DB.WithContext(ctx).Where("file_name = ?", fileName).First(&md.File{}).RowsAffected
	if row == 0 {
		return nil, errors.New("row not found")
	}
	return file, nil
}

// UpdateFile: 更新文件表
func UpdateFile(ctx context.Context, fileSha1, fileAddress string) error {
	return md.DB.WithContext(ctx).Model(&md.File{}).Updates(map[string]interface{}{
		"file_sha1":    fileSha1,
		"file_address": fileAddress,
	}).Error
}

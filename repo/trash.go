package repo

import (
	"context"
	md "file-store/models"
	"file-store/utils"
	"os"
	"time"
)

// CreateTrash: 创建垃圾文件或文件夹
func CreateTrash(ctx context.Context, name, fileSha1 string, fileType, folder_id, userId int64) error {
	trash := &md.Trash{
		Name:      name,
		Type:      fileType,
		FileSha1:  fileSha1,
		FolderId:  folder_id,
		UserId:    userId,
		CreatedAt: utils.FormatTime(time.Now()),
	}
	return md.DB.WithContext(ctx).Create(trash).Error
}

// DeleteFileById: 根据userId和id删除指定的垃圾文件
func DeleteTrashFileById(ctx context.Context, userId, fileType, folderId, id int64, fileSha1 string) error {
	tx := md.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if tx.Error != nil {
		return tx.Error
	}
	// 删除垃圾箱中指定的文件记录
	if err := md.DB.WithContext(ctx).Where("user_id = ? And id = ?", userId, id).
		Delete(&md.Trash{}).Error; err != nil {
		tx.Rollback()
		return err
	}
	// 判断要删除的是文件还是文件夹
	if fileType == 0 {
		// 文件夹操作
		if err := DeleteUserFolder(ctx, folderId); err != nil {
			tx.Rollback()
			return err
		}
	} else {
		// 查询文件的存储地址
		file, err := FindFileByFileSha1(ctx, fileSha1)
		if err != nil {
			tx.Rollback()
			return err
		}
		// 如果是文件则先删除用户文件表中的记录
		if err := DeleteUserFileByFileSha1(ctx, fileSha1, userId); err != nil {
			tx.Rollback()
			return err
		}
		// 再删除文件表中的文件记录
		if err := DeleteFileByFilehash(ctx, fileSha1); err != nil {
			tx.Rollback()
			return err
		}
		// 最后直接磁盘中的文件
		if err := os.Remove(file.FileAddress); err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit().Error
}

// DeleteFindById: 清空指定用户垃圾箱内的所有垃圾文件和文件夹
func DeleteAllById(ctx context.Context, userId int64) error {
	return md.DB.WithContext(ctx).Where("user_id = ?", userId).
		Delete(&md.Trash{}).Error
}

// FindTrashFilesById: 根据用户id查找所有的垃圾文件
func FindTrashFilesById(ctx context.Context, userId int64) (files []*md.Trash, err error) {
	return files, md.DB.WithContext(ctx).Where("user_id = ?", userId).Find(&files).Error
}

// RestoreTrashFile：还原垃圾箱中指定的文件或文件夹
func RestoreTrashFile(ctx context.Context, userId, fileType, folderId, id, status int64, fileSha1 string) error {
	tx := md.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if tx.Error != nil {
		tx.Rollback()
		return tx.Error
	}

	// 删除垃圾箱中的记录
	if err := md.DB.WithContext(ctx).Where("user_id = ? And id = ?", userId, id).
		Delete(&md.Trash{}).Error; err != nil {
		tx.Rollback()
		return err
	}

	if fileType == 0 {
		// 文件夹
		if err := UpdateUserFolderStatus(ctx, userId, folderId, status); err != nil {
			tx.Rollback()
			return err
		}
	} else {
		// 文件
		if err := UpdateUserFileStatus(ctx, userId, status, fileSha1); err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit().Error
}

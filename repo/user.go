package repo

import (
	"context"
	md "file-store/models"
	"file-store/utils"
	"time"
)

// CreateUser: 注册用户
func CreateUser(ctx context.Context, userName, password, phone string) (
	id int64, err error) {
	user := &md.User{
		UserName:  userName,
		Password:  password,
		Phone:     phone,
		CreatedAt: utils.FormatTime(time.Now()),
	}
	return user.ID, md.DB.Create(user).Error
}

// FindUserByUserName: 根据user_name查找用户数据
func FindUserByUserName(ctx context.Context, userName string) (*md.User, error) {
	user := &md.User{}
	return user, md.DB.Where("user_name = ?", userName).First(user).Error
}

// FindUserById: 根据id查找用户数据
func FindUserById(ctx context.Context, id int64) (*md.User, error) {
	user := &md.User{}
	return user, md.DB.First(&user, id).Error
}

// UpdateLastActiveById: 根据id更新登陆时间
func UpdateLastActiveById(ctx context.Context, id int64) error {
	lastActive := utils.FormatTime(time.Now())
	return md.DB.WithContext(ctx).Model(&md.User{}).Where("id", id).
		Update("last_active", lastActive).Error
}

// UpdateUserPasswordById: 根据id更新用户密码
func UpdateUserPasswordById(ctx context.Context, newPassword string, id int64) error {
	return md.DB.WithContext(ctx).Model(&md.User{}).Where("id", id).
		Update("password", newPassword).Error
}

// UpdateUser: 更新用户信息
func UpdateUser(ctx context.Context, id int64, userName, profile, phone string) error {
	return md.DB.WithContext(ctx).Model(&md.User{}).Where("id = ?", id).
		Updates(map[string]interface{}{
			"user_name": userName,
			"profile":   profile,
			"phone":     phone,
		}).Error
}

// DeleteUserById: 根据id删除用户信息
func DeleteUserById(ctx context.Context, id int64) error {
	return md.DB.WithContext(ctx).Delete(&md.User{}, id).Error
}

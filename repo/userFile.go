package repo

import (
	"context"
	md "file-store/models"
	"file-store/utils"
	"time"
)

// CreateUserFile: 创建用户文件数据
func CreateUserFile(ctx context.Context, userId int64, fileSha1, fileName, fileType, fileSize string,
	folderId int64) (id int64, err error) {
	userFile := md.UserFile{
		UserID:    userId,
		FileSha1:  fileSha1,
		FileName:  fileName,
		FileSize:  fileSize,
		FolderID:  folderId,
		Type:      fileType,
		UploadAt:  utils.FormatTime(time.Now()),
		CreatedAt: utils.FormatTime(time.Now()),
	}
	return userFile.ID, md.DB.WithContext(ctx).Create(&userFile).Error
}

// FindUserFileByNameAndSha1: 根据file_sha1和user_name查询数据
func FindUserFileByUserAndSha1(ctx context.Context, userId int64, fileSha1 string) (
	*md.UserFile, error) {
	userFile := &md.UserFile{}
	return userFile, md.DB.WithContext(ctx).Where("user_id = ? And file_sha1 = ?",
		userId, fileSha1).Find(userFile).Error
}

// 根据user_id和folder_id查找指定文件夹下的所有文件信息
func FindFilesByUserAndFolder(ctx context.Context, userId, folderId int64) (files []*md.UserFile,
	err error) {
	return files, md.DB.WithContext(ctx).
		Where("user_id = ? And folder_id = ? And status = ?", userId, folderId, 0).
		Find(&files).Error
}

// UpdarteStatusByFileSha1AndUserId: 根据userId和fileSha1修改文件的状态
func UpdateStatusByFileSha1AndUserId(ctx context.Context, name, fileSha1 string, userId,
	fileType, folderId, status int64) error {
	now := utils.FormatTime(time.Now())
	tx := md.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if tx.Error != nil {
		return tx.Error
	}
	// 更新用户文件表中的文件状态
	userFileSql := "UPDATE user_files SET status = ?, updated_at = ? WHERE user_id = ? AND file_sha1 = ?"
	if err := md.DB.WithContext(ctx).
		Exec(userFileSql, status, now, userId, fileSha1).
		Error; err != nil {
		tx.Rollback()
		return err
	}
	// 创建新的垃圾文件
	trashSql := "insert into trashes (name, type, file_sha1, folder_id, user_id, created_at) values (?, ?, ?, ?, ?, ?)"
	if err := md.DB.WithContext(ctx).Exec(trashSql, name, fileType, fileSha1, folderId, userId, now).
		Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

// DeleteUserFileByFileSha1: 根据file_sha1删除指定的用户文件信息
func DeleteUserFileByFileSha1(ctx context.Context, fileSha1 string, userId int64) error {
	return md.DB.WithContext(ctx).
		Exec("DELETE FROM user_files WHERE file_sha1 = ? AND user_id = ?", fileSha1, userId).
		Error
}

// UpdateFileName: 文件重命名
func UpdateFileName(ctx context.Context, fileSha1, fileName string) (name, lastUpdate string, err error) {
	userFile := &md.UserFile{
		FileName:   fileName,
		LastUpdate: utils.FormatTime(time.Now()),
		UpdatedAt:  utils.FormatTime(time.Now()),
	}
	return userFile.FileName, userFile.LastUpdate, md.DB.WithContext(ctx).Model(&userFile).
		Where("file_sha1 = ?", fileSha1).Updates(userFile).Error
}

// MoveFileToFolder: 移动文件到指定的文件夹
func MoveFileToFolder(ctx context.Context, fileSha1 string, folderId int64) error {
	return md.DB.WithContext(ctx).Model(&md.UserFile{}).Where("file_sha1 = ?", fileSha1).
		Update("folder_id", folderId).Error
}

// UpdateUserFileStatus: 更新用户文件的状态
func UpdateUserFileStatus(ctx context.Context, userId, status int64, fileSha1 string) error {
	return md.DB.WithContext(ctx).Model(&md.UserFile{}).
		Where("user_id = ? And file_sha1 = ?", userId, fileSha1).
		Update("status", status).Error
}

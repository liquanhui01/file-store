package repo

import (
	"context"
	md "file-store/models"
	"file-store/utils"
	"time"
)

// CreateUserFolder: 创建用户文件夹
func CreateUserFolder(ctx context.Context, userId int64, folderName string) (uf *md.UserFolder, err error) {
	uf = &md.UserFolder{
		FolderName: folderName,
		UserId:     userId,
		Status:     0,
		CreatedAt:  utils.FormatTime(time.Now()),
		UpdatedAt:  utils.FormatTime(time.Now()),
	}
	return uf, md.DB.WithContext(ctx).Create(uf).Error
}

// DeleteUserFolderById: 根据userId和folderId修改用户文件夹状态
func UpdateUserFolderStatus(ctx context.Context, userId, folderId, status int64) error {
	return md.DB.WithContext(ctx).Model(&md.UserFolder{}).
		Where("user_id = ? AND id = ?", userId, folderId).
		Update("status", status).Error
}

// DeleteUserFolder: 根据folderId和userId彻底删除文件夹
func DeleteUserFolder(ctx context.Context, folderId int64) error {
	return md.DB.WithContext(ctx).Delete(&md.UserFolder{}, folderId).Error
	// return md.DB.WithContext(ctx).Exec("DELETE FROM user_folders WHERE folder_id = ?", folderId).Error
}

// FindFolderByUserId: 根据userId查询用户所有的文件夹
func FindFolderByUserId(ctx context.Context, userId int64) (userFolders []*md.UserFolder,
	err error) {
	return userFolders, md.DB.WithContext(ctx).Where("user_id = ? And status = 0", userId).
		Find(&userFolders).Error
}

// UpdateFolderByUserId: 根据user_id更新用户文件夹文件名称
func UpdateFolderByUserId(ctx context.Context, userId, folderId int64, folderName string) (name string,
	err error) {
	user := &md.UserFolder{
		FolderName: folderName,
		UpdatedAt:  utils.FormatTime(time.Now()),
	}
	return user.FolderName, md.DB.WithContext(ctx).Model(user).
		Where("user_id = ? AND id = ?", userId, folderId).
		Updates(user).Error
}

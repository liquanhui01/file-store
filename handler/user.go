package handler

import (
	"context"
	se "file-store/service"
	"file-store/utils"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateUserHandler: 创建用户
func CreateUserHandler(c *gin.Context) {
	userName := c.Request.FormValue("user_name")
	password := c.Request.FormValue("password")
	phone := c.Request.FormValue("phone")
	_, err := se.CreateUser(c, userName, password, phone)
	if err != nil {
		fmt.Printf("Filed to create user, err is %s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "注册失败",
			"data": nil,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": 200,
			"msg":  "注册成功",
			"data": nil,
		})
	}
}

// LoginHandler: 用户登陆
func LoginHandler(c *gin.Context) {
	userName := c.Request.FormValue("user_name")
	password := c.Request.FormValue("password")
	fmt.Printf("用户名和密码为: %s, %s\n", userName, password)
	user, token, err := se.UserLogin(c, userName, password)
	if err != nil {
		fmt.Printf("Failed to login, err is %s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "登陆失败",
			"data": nil,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": 200,
			"msg":  "登陆成功",
			"data": struct {
				UserId   int64
				Token    string
				UserName string
				Avatar   string
			}{
				UserId:   user.ID,
				Token:    token,
				UserName: user.UserName,
				Avatar:   user.Avatar,
			},
		})
	}
}

// FindUserByIdHandler: 根据id查找用户信息
func FindUserByIdHandler(c *gin.Context) {
	id, _ := strconv.ParseInt(c.Request.FormValue("id"), 10, 64)
	ctx := context.Background()
	user, err := se.FindUserById(ctx, id)
	if err != nil {
		fmt.Printf("查找失败：错误为：%s\n", err)
		c.JSON(http.StatusOK, gin.H{
			"code": 0,
			"msg":  "获取失败",
			"data": user,
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "获取成功",
		"data": user,
	})
}

// DeleteUserByIdHandler: 根据id删除用户信息
func DeleteUserByIdHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	id, _ := strconv.ParseInt(r.Form.Get("id"), 10, 64)
	ctx := context.Background()
	err := se.DeleteUserById(ctx, id)
	if err != nil {
		fmt.Printf("删除失败，错误为：%s\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	resp := utils.Response{
		Code: 200,
		Msg:  "OK",
		Data: "删除成功",
	}
	w.Write(resp.JSONBytes())
}

func UpdateUserPasswordHandler(c *gin.Context) {
	newPassword := c.Request.FormValue("new_password")
	id, _ := strconv.ParseInt(c.Request.FormValue("id"), 10, 64)
	fmt.Printf(newPassword, id)
	if len(newPassword) < 6 {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 1,
			"msg":  "密码格式不正确",
		})
	}
	password, err := utils.GenerateHashPassword(newPassword)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 2,
			"msg":  "服务端错误",
		})
	}
	err = se.UpdateUserPasswordById(c, string(password), id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 1,
			"msg":  "密码格式不正确",
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "修改成功",
	})
}

// 更新用户数据
func UpdateUserInfoHandler(c *gin.Context) {
	id, _ := strconv.ParseInt(c.Request.FormValue("id"), 10, 64)
	userName := c.Request.FormValue("user_name")
	phone := c.Request.FormValue("phone")
	profile := c.Request.FormValue("profile")
	err := se.UpdateUser(c, id, userName, profile, phone)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 2,
			"msg":  "更新失败",
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": 200,
			"msg":  "更新成功",
		})
	}
}

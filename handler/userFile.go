package handler

import (
	se "file-store/service"
	"file-store/utils"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func FindFolderFilesHandler(c *gin.Context) {
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	folderId, _ := strconv.ParseInt(c.Request.FormValue("folder_id"), 10, 64)
	fmt.Printf("userId: %d, folderId: %d\n", userId, folderId)
	files, err := se.FindFilesByUserAndFolder(c, userId, folderId)
	fmt.Printf("获取的结果为：%v\n", files)
	if err != nil {
		fmt.Printf("Failed to find files, err is: %s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "查询失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "查询成功",
		"data": files,
	})
}

// UpdateFileNameHandler: 文件重命名
func UpdateFileNameHandler(c *gin.Context) {
	fileSha1 := c.Request.FormValue("file_sha1")
	newFileName := c.Request.FormValue("new_file_name")
	originFileName := c.Request.FormValue("origin_file_name")
	fileName := utils.RenameFile(newFileName, originFileName)
	name, lastUpdate, err := se.UpdateFileName(c, fileSha1, fileName)
	if err != nil {
		fmt.Printf("Failed to update fileName, err is:%s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "重命名失败",
			"data": nil,
		})
		return
	}
	err = se.UpdateFileNameByFilehash(c, fileSha1, fileName)
	if err != nil {
		fmt.Printf("Failed to update fileName, err is:%s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "重命名失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "重命名成功",
		"data": struct {
			FileName   string
			LastUpdate string
		}{
			FileName:   name,
			LastUpdate: lastUpdate,
		},
	})
}

// MoveFileToFolderHandler: 移动文件到指定的文件夹
func MoveFileToFolderHandler(c *gin.Context) {
	fileSha1 := c.Request.FormValue("file_sha1")
	folderId, _ := strconv.ParseInt(c.Request.FormValue("folder_id"), 10, 64)
	err := se.MoveFileToFolder(c, fileSha1, folderId)
	if err != nil {
		fmt.Printf("Failed to move file, err is :%s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "移动失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "移动成功",
		"data": nil,
	})
}

// UpdateStatusHandler: 修改文件的状态
func UpdateStatusHandler(c *gin.Context) {
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	// status, _ := strconv.ParseInt(c.Request.FormValue("status"), 10, 64)
	fileSha1 := c.Request.FormValue("file_sha1")
	fileName := c.Request.FormValue("file_name")
	folderId, _ := strconv.ParseInt(c.Request.FormValue("folder_id"), 10, 64)
	err := se.UpdateStatusByFileSha1AndUserId(c, fileName, fileSha1, userId, 1, folderId, 1)
	if err != nil {
		fmt.Printf("Failed to modify file's status, err is :%s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "更新失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "更新成功",
		"data": nil,
	})
}

package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"file-store/common"
	"file-store/kafka"
	se "file-store/service"
	oss "file-store/store/oss"
	"file-store/utils"
)

func UploadHandler(c *gin.Context) {
	var (
		fileSha1 string
	)
	// 接收文件流，及存储到本地目录
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		fmt.Printf("Failed to get data, err:%s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "上传失败",
			"data": nil,
		})
		return
	}
	defer file.Close()

	// 判断文件类型
	fileType := utils.CheckFileType(header.Filename)
	if fileType == "" {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "不支持当前文件类型",
			"data": nil,
		})
		return
	}
	// 文件元数据
	fileAddress := utils.FormatFilePath(header.Filename)
	_, fileSha1, fileSize, err := utils.UploadFile(fileAddress, file)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "上传失败",
			"data": nil,
		})
		return
	}

	// 判断写入OSS是异步还是同步
	ossPath := utils.SpliceString("test/", fileSha1)
	// if !conf.Rq.AsyncTransferEnable {
	// 	// 文件写入OSS存储
	// 	ossPath := utils.SpliceString("test/", fileSha1)
	// 	err = oss.Bucket().PutObjectFromFile(ossPath, fileAddress)
	// 	if err != nil {
	// 		fmt.Printf("Failed to save file with oss, err is %s\n", err.Error())
	// 		c.JSON(http.StatusInternalServerError, gin.H{
	// 			"code": 0,
	// 			"msg":  "上传失败",
	// 			"data": nil,
	// 		})
	// 		return
	// 	}
	// } else {
	// 文件尚未转移，暂存于本地
	data := &kafka.TransferData{
		FileSha1:      fileSha1,
		CurAddress:    fileAddress,
		DestAddress:   ossPath,
		DestStoreType: common.StoreOSS,
	}
	suc := kafka.Publish(data)
	if !suc {
		// TODO: 加入重试消息的逻辑
	}
	// }

	var status int64
	fileMeta := utils.FormatFileMate(header.Filename, fileSha1, ossPath, fileSize, status)

	// 创建文件元数据信息
	_, err = se.CreateFile(c, fileMeta)
	if err != nil {
		fmt.Printf("Failed to save file, err:%s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "上传失败",
			"data": nil,
		})
		return
	}
	// 添加user_files记录
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	folderId, _ := strconv.ParseInt(c.Request.FormValue("folder_id"), 10, 64)
	_, err = se.CreateUserFile(c, userId, fileSha1, fileMeta.FileName, fileType, fileMeta.FileSize, folderId)
	if err != nil {
		fmt.Printf("Failed to save file, err:%s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "上传失败",
			"data": nil,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "上传成功",
		"data": nil,
	})
}

// GetFileMetaHandler: 获取文件的元数据
func GetFileMetaHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	filehash := r.Form["filehash"][0]
	// userId, _ := strconv.ParseInt(r.Form.Get("user_name"), 10, 64)
	ctx := context.Background()
	fMeta, err := se.FindFileMetaByFileSha1(ctx, filehash)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(fMeta)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(data)
}

// FileQueryHandler: 查询多个文件的元数据
func FileQueryHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	limitCnt, _ := strconv.ParseInt(r.Form.Get("limit"), 10, 64)
	ctx := context.Background()
	results, err := se.FindFileQueryByLimit(ctx, limitCnt)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(results)
	fmt.Println(results)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(data)
}

// DownloadHandler文件下载接口
func DownloadHandler(c *gin.Context) {
	fileSha1 := c.Request.FormValue("file_sha1")
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	userFile, err := se.FindUserFileByUserAndSha1(c, userId, fileSha1)
	if err != nil {
		fmt.Printf("Failed to find file, err is %s\n", err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"code": 0,
			"msg":  "找不到文件",
			"data": nil,
		})
		return
	}
	fm, err := se.FindFileMetaByFileSha1(c, fileSha1)
	if err != nil {
		fmt.Printf("Failed to find file, err is %s\n", err.Error())
		c.JSON(http.StatusNotFound, gin.H{
			"code": 0,
			"msg":  "找不到文件",
			"data": nil,
		})
		return
	}

	c.Header("Content-Type", "application/octet-stream")
	// 强制浏览器下载
	c.Header("Content-Disposition", "attachment;filename=\""+userFile.FileName+"\"")
	// 浏览器下载或预览
	c.Header("Content-Disposition", "inline;filename=\""+userFile.FileName+"\"")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Cache-Control", "no-cache")
	c.Header("Access-Control-Expose-Headers", "Content-Disposition")
	c.Header("response-type", "blob") // 以流的形式下载必须设置这一项，否则前端下载下来的文件会出现格式不正确或已损坏的问题
	c.File(fm.FileAddress)            // 这一项如果不填写会显示文件内容为空
}

// FileMetaUpdateHandler: 修改文件的元数据信息(重命名)
func FileMetaUpdateHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	opType := r.Form.Get("op")
	filehash := r.Form.Get("filehash")
	filename := r.Form.Get("filename")
	if opType != "0" {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	ctx := context.Background()
	err := se.UpdateFileNameByFilehash(ctx, filehash, filename)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

// SecondPassHandler: 秒传文件的处理
func SecondPassHandler(c *gin.Context) {
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	folderId, _ := strconv.ParseInt(c.Request.FormValue("folder_id"), 10, 64)
	fileName := c.Request.FormValue("file_name")
	fileSha1 := c.Request.FormValue("file_sha1")
	file, err := se.FindFileMetaByFileSha1(c, fileSha1)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "秒传失败",
			"data": nil,
		})
		return
	}
	fileType := utils.CheckFileType(fileName)
	_, err = se.CreateUserFile(c, userId, fileSha1, fileName, fileType, file.FileSize, folderId)
	if err != nil {
		fmt.Printf("Failed to save file, err:%s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "秒传失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "秒传成功",
		"data": nil,
	})
}

// DownloadURLHandler: 生成文件的下载地址
func DownloadURLHandler(c *gin.Context) {
	fileSha1 := c.Request.FormValue("file_sha1")
	// 从文件表中查找文件的元数据
	file, err := se.FindFileMetaByFileSha1(c, fileSha1)
	if err != nil {
		fmt.Printf("Failed to find file meta, err is: %s", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "下载失败",
			"data": nil,
		})
		return
	}
	// TODO 判断文件是存储在ceph还是OSS中

	signedURL := oss.DownloadURL(file.FileAddress)
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "成功",
		"data": signedURL,
	})
}

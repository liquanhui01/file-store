package handler

import (
	"file-store/cache/redis"
	conf "file-store/config"
	utils "file-store/utils"
	"fmt"
	"math"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	redigo "github.com/garyburd/redigo/redis"
	"github.com/gin-gonic/gin"
)

// 初始化信息
type MultipartUploadInfo struct {
	FileSha1   string
	FileSize   int
	UploadID   string
	ChunkSize  int // 每一个分块的大小
	ChunkCount int // 分块的数量
}

// InitialMultipartUploadHandler: 初始化分块上传
// 1.获取用户文件上传的参数
// 2.获取一个redis链接
// 3.生成分块上传的初始化信息
// 4.将初始化信息写入到redis中
// 5.将初始化的数据返回到客户端
func InitialMultipartUploadHandler(c *gin.Context) {
	userId := c.Request.FormValue("user_id")
	fileSize, _ := strconv.Atoi(c.Request.FormValue("file_size"))
	fileSha1 := c.Request.FormValue("file_sha1")

	// 获取一个链接
	rConn := redis.RedisPool().Get()
	defer rConn.Close()
	uploadID := utils.SpliceString(userId, fmt.Sprintf("%x", time.Now().UnixNano()))
	upInfo := MultipartUploadInfo{
		FileSha1:   fileSha1,
		FileSize:   fileSize,
		UploadID:   uploadID,
		ChunkSize:  5 * 1024 * 1024,
		ChunkCount: int(math.Ceil(float64(fileSize) / (5 * 1024 * 1024))), // 先转成float除后向上取整，最后转换为int类型
	}

	rConn.Do("HSET", "MP_"+upInfo.UploadID, "chunkcount", upInfo.ChunkCount)
	rConn.Do("HSET", "MP_"+upInfo.UploadID, "filesha1", upInfo.FileSha1)
	rConn.Do("HSET", "MP_"+upInfo.UploadID, "filesize", upInfo.FileSize)

	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "初始化成功",
		"data": upInfo,
	})
}

// 上传文件分块
func UploadPartHander(c *gin.Context) {
	// userId := c.Request.FormValue("user_id")
	uploadID := c.Request.FormValue("upload_id")
	chunkIndex := c.Request.FormValue("index")
	// 获取一个链接
	rConn := redis.RedisPool().Get()
	defer rConn.Close()

	fmt.Println("参数为：", uploadID, chunkIndex)

	// 获取文件句柄，用于存储分块的内容
	fPath := conf.GlobalConfig.File.Path + uploadID + "/" + chunkIndex
	fmt.Print(fPath)
	os.MkdirAll(path.Dir(fPath), 0744) // 创建目录
	fd, err := os.Create(fPath)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": -1,
			"msg":  "分块上传失败",
			"data": nil,
		})
		return
	}
	defer fd.Close()
	buf := make([]byte, 1024*1024)
	for {
		n, err := c.Request.Body.Read(buf)
		fd.Write(buf[:n])
		if err != nil {
			break
		}
	}

	// 跟新redis缓存状态
	rConn.Do("HSET", "MP_"+uploadID, "chunkIndex_"+chunkIndex, 1)

	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "分块上传成功",
		"data": nil,
	})
}

// CompleteUploadHandler: 通知上传合并接口
// 1. 获取用户请求参数
// 2. 获取redis连接池中的一个链接
// 3. 通过uploadID查询redis判断所有分块是否上传完成
// 4. TODO: 合并分块
// 5. 更新文件表和用户文件表
// 6. 翻译结果给客户端
func CompleteUploadHandler(c *gin.Context) {
	uploadID := c.Request.FormValue("upload_id")
	// userId := c.Request.FormValue("user_id")
	// fileSha1 := c.Request.FormValue("file_sha1")
	// fileSize := c.Request.FormValue("file_size")
	// fileName := c.Request.FormValue("file_name")

	// 获取一个redis链接
	rConn := redis.RedisPool().Get()
	defer rConn.Close()
	data, err := redigo.Values(rConn.Do("HGETALL", "MP_"+uploadID))
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "分块合并失败",
			"data": nil,
		})
		return
	}

	totalCount := 0
	chunkCount := 0
	// 因为是通过HGETALL的方式获取，data里同时存有k,v，各占一个元素，所以步长为2
	for i := 0; i < len(data); i += 2 {
		k := string(data[i].([]byte))
		v := string(data[i].([]byte))
		if k == "chunkcount" {
			totalCount, _ = strconv.Atoi(v)
		} else if strings.HasSuffix(k, "chunkIndex_") && v == "1" {
			chunkCount++
		}
	}
	if totalCount != chunkCount {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": -1,
			"msg":  "分块上传失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "分块上传完成",
		"data": nil,
	})
}

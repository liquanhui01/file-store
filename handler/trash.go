package handler

import (
	se "file-store/service"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// DeleteFileHandler: 彻底删除指定的文件或者文件夹
func DeleteTrashFileHandler(c *gin.Context) {
	id, _ := strconv.ParseInt(c.Request.FormValue("id"), 10, 64)
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	fileType, _ := strconv.ParseInt(c.Request.FormValue("type"), 10, 64)
	folderId, _ := strconv.ParseInt(c.Request.FormValue("folder_id"), 10, 64)
	fileSha1 := c.Request.FormValue("file_sha1")
	if err := se.DeleteTrashFileById(c, userId, fileType, folderId, id, fileSha1); err != nil {
		fmt.Printf("Failed to delete trash file, err is: %s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "删除失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "删除成功",
		"data": nil,
	})
}

// FindTrashFilesHandler: 查找所有的垃圾文件或文件夹
func FindTrashFilesHandler(c *gin.Context) {
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	userFiles, err := se.FindTrashFilesById(c, userId)
	if err != nil {
		fmt.Printf("Failed to find all trash file, err is: %s\n", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "查找失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "查找成功",
		"data": userFiles,
	})
}

// RestoreTrashFileHandler: 还原指定的文件或文件夹
func RestoreTrashFileHandler(c *gin.Context) {
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	fileType, _ := strconv.ParseInt(c.Request.FormValue("type"), 10, 64)
	folderId, _ := strconv.ParseInt(c.Request.FormValue("folder_id"), 10, 64)
	id, _ := strconv.ParseInt(c.Request.FormValue("id"), 10, 64)
	fileSha1 := c.Request.FormValue("file_sha1")
	if err := se.RestoreTrashFile(c, userId, fileType, folderId, id, 0, fileSha1); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "还原失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "还原成功",
		"data": nil,
	})
}

package handler

import (
	se "file-store/service"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateUserFolderHandler: 创建用户文件夹
func CreateUserFolderHandler(c *gin.Context) {
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	folderName := c.Request.FormValue("folder_name")
	folder, err := se.CreateUserFolder(c, userId, folderName)
	if err != nil {
		fmt.Printf("Failed to create folder, err is: %s", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "文件夹创建失败",
			"data": nil,
		})
	} else {
		c.JSON(http.StatusCreated, gin.H{
			"code": 201,
			"msg":  "创建成功",
			"data": folder,
		})
	}
}

// DeleteUserFolderHandler: 根据userId删除指定的用户文件夹
func DeleteUserFolderHandler(c *gin.Context) {
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	folderId, _ := strconv.ParseInt(c.Request.FormValue("folder_id"), 10, 64)
	folderName := c.Request.FormValue("folder_name")
	if err := se.DeleteUserFolderById(c, userId, folderId, 1); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "删除失败",
			"data": nil,
		})
		return
	}
	if err := se.CreateTrash(c, folderName, "", 0, folderId, userId); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "删除失败",
			"data": nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "删除成功",
		"data": nil,
	})
}

// FindUserFolderHandler: 查询用户所有的文件夹
func FindUserFolderHandler(c *gin.Context) {
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	folders, err := se.FindFolderByUserId(c, userId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "查询失败",
			"data": nil,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": 200,
			"msg":  "查询成功",
			"data": folders,
		})
	}
}

// UpdateFolderHandler: 更新文件夹名称
func UpdateFolderHandler(c *gin.Context) {
	userId, _ := strconv.ParseInt(c.Request.FormValue("user_id"), 10, 64)
	folderId, _ := strconv.ParseInt(c.Request.FormValue("folder_id"), 10, 64)
	folderName := c.Request.FormValue("folder_name")
	fmt.Printf("数据为: %d, %d, %s\n", userId, folderId, folderName)
	newFolderName, err := se.UpdateFolderByUserId(c, userId, folderId, folderName)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 0,
			"msg":  "更新失败",
			"data": nil,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": 200,
			"msg":  "更新成功",
			"data": newFolderName,
		})
	}
}

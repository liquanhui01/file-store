package middleware

import (
	"file-store/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

// HTTPInterceptor：请求拦截器，进行参数校验和token的认证
func Authorize() gin.HandlerFunc {
	return gin.HandlerFunc(
		func(c *gin.Context) {
			userName := c.Request.FormValue("user_name")
			token := c.GetHeader("Authorization")
			if userName != "" {
				if len(userName) < 3 || len(userName) > 20 {
					c.Abort()
					c.JSON(http.StatusUnauthorized, gin.H{
						"code": 1,
						"msg":  "用户名格式不正确",
					})
					return
				} else {
					c.Next()
				}
			}
			if token != "" {
				if _, err := utils.ParseToken(token); err != nil {
					c.Abort()
					c.JSON(http.StatusUnauthorized, gin.H{
						"code": 0,
						"msg":  "访问未授权",
					})
					return
				} else {
					c.Next()
				}
			}
		})
}

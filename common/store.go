package common

type StoreType int

const (
	_ StoreType = iota
	// StoreLocal: 节点本地存储
	StoreLocal
	// StoreCeph: Ceph集群存储
	StoreCeph
	// StoreOSS: 阿里云OSS
	StoreOSS
	// StoreMix: 混合(Ceph及Ceph)
	StoreMix
	// StoreAll: 所有类型的存储都存一份
	StoreAll
)

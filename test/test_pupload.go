package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"

	jsonit "github.com/json-iterator/go"
)

func multipartUpload(filename string, targetURL string, chunkSize int) error {
	f, err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer f.Close()

	bfRd := bufio.NewReader(f)
	index := 0

	ch := make(chan int)
	buf := make([]byte, chunkSize) //每次读取chunkSize大小的内容
	for {
		n, err := bfRd.Read(buf)
		if n <= 0 {
			break
		}
		index++

		bufCopied := make([]byte, 5*1048576)
		copy(bufCopied, buf)

		go func(b []byte, curIdx int) {
			fmt.Printf("upload_size: %d\n", len(b))

			resp, err := http.Post(
				targetURL+"&index="+strconv.Itoa(curIdx),
				"multipart/form-data",
				bytes.NewReader(b))
			if err != nil {
				fmt.Println(err)
			}

			body, er := ioutil.ReadAll(resp.Body)
			fmt.Printf("%+v %+v\n", string(body), er)
			resp.Body.Close()

			ch <- curIdx
		}(bufCopied[:n], index)

		//遇到任何错误立即返回，并忽略 EOF 错误信息
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println(err.Error())
			}
		}
	}

	for idx := 0; idx < index; idx++ {
		select {
		case res := <-ch:
			fmt.Println(res)
		}
	}

	return nil
}

func main() {
	userId := 2
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJGbGFnIjoiYzgxZTcyOGQ5ZDRjMmY2MzZmMDY3Zjg5Y2MxNDg2MmMiLCJleHAiOjE2MzA5ODQxMzksImp0aSI6ImM4MWU3MjhkOWQ0YzJmNjM2ZjA2N2Y4OWNjMTQ4NjJjIiwiaWF0IjoxNjMwOTgxMTM5LCJpc3MiOiIxMjcuMC4wLjEiLCJuYmYiOjE2MzA5ODExMzksInN1YiI6InVzZXIgdG9rZW4ifQ.T4Crvuv_chEidFv-rKUGq_b8quiSBpb8lwUnH0Sg1Cc"
	fileSha1 := "69bac8a043454d535dacecc07e24512573100782"

	// 请求初始化上传接口
	resp, err := http.PostForm(
		"http://127.0.0.1:8000/api/v1/token/file/mpupload/init",
		url.Values{
			"user_id":   {strconv.Itoa(userId)},
			"token":     {token},
			"file_sha1": {fileSha1},
			"file_size": {"9214975"},
		},
	)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}

	// 得到uploadID以及服务端指定的分块大小chunkSize
	uploadID := jsonit.Get(body, "data").Get("UploadID").ToString()
	chunkSize := jsonit.Get(body, "data").Get("ChunkSize").ToInt()
	fmt.Printf("uploadid: %s  chunksize: %d\n", uploadID, chunkSize)

	// 请求分块上传接口
	fileName := "/Users/apple/Downloads/b748d5b286ff1c044e2d78d3be0ee620.mp4"
	tURL := "http://127.0.0.1:8000/api/v1/token/file/mpupload/uppart?" +
		"user_id = 2&token=" + token + "&upload_id=" + uploadID
	multipartUpload(fileName, tURL, chunkSize)

	// 4. 请求分块完成接口
	resp, err = http.PostForm(
		"http://localhost:8080/api/v1/token/file/mpupload/complete",
		url.Values{
			"user_id":   {strconv.Itoa(userId)},
			"token":     {token},
			"file_sha1": {fileSha1},
			"file_size": {"9214975"},
			"file_name": {fileName},
			"upload_id": {uploadID},
		})

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}
	fmt.Printf("complete result: %s\n", string(body))
}

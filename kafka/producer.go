package kafka

import (
	"encoding/json"
	"log"
	"sync"
	"time"

	"github.com/Shopify/sarama"
)

var (
	address []string = []string{"127.0.0.1:19092"}
	Async   string   = "Async"
	Sync    string   = "Sync"
	pro     sarama.SyncProducer
)

func InitKafka() {
	config := sarama.NewConfig()
	// 确定返回，即同步操作，需要写
	config.Producer.Return.Successes = true
	// 设置超时时间，这个超时时间一过期，新的订阅者在这个超时时间后才创建的，就不能订阅消息了
	config.Producer.Timeout = 5 * time.Second

	config.Producer.Partitioner = sarama.NewManualPartitioner
	// 连接发布者，并创建发布者实例
	pro, err := sarama.NewSyncProducer(address, config)
	if err != nil {
		return
	}
	defer pro.Close()
}

func Publish(mg *TransferData) bool {
	topic := "file"
	value, _ := json.Marshal(mg)
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		msg := &sarama.ProducerMessage{
			Topic: topic,
			Value: sarama.StringEncoder(value),
		}
		partition, offset, err := pro.SendMessage(msg)
		if err != nil {
			log.Printf("发送消息失败，错误为：%s\n", err.Error())
			return
		} else {
			log.Printf("发送成功, partition=%d, offset=%d\n", partition, offset)
		}
	}()
	wg.Wait()
	return true
}

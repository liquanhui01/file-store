package kafka

import (
	"file-store/common"
)

type TransferData struct {
	FileSha1      string           `json:"file_sha1"`
	CurAddress    string           `json:"cur_Address"`
	DestAddress   string           `json:"dest_Address"`
	DestStoreType common.StoreType `json:"dest_store_type"`
}

package route

import (
	"file-store/handler"
	mdw "file-store/middleware"

	"github.com/gin-gonic/gin"
)

func Router() *gin.Engine {
	// gin框架，包括Logger, Recovery
	router := gin.Default()
	router.Use(mdw.Cors())         // 开启中间件，允许使用跨域请求
	router.Use(mdw.LoggerToFile()) // 开启日志中间件
	v1 := router.Group("/api/v1")
	{
		// 不验证的接口
		v1.POST("/user/register", handler.CreateUserHandler)
		v1.POST("/user/login", handler.LoginHandler)

		// 加入验证的接口
		token := v1.Group("/token")
		{
			// 加入auth认证中间件
			token.Use(mdw.Authorize())
			// 用户路由
			user := token.Group("/user")
			{
				user.GET("/find", handler.FindUserByIdHandler)
				user.PUT("/password", handler.UpdateUserPasswordHandler)
				user.PUT("/update", handler.UpdateUserInfoHandler)
			}
			// 文件夹路由
			folder := token.Group("/folder")
			{
				folder.POST("/", handler.CreateUserFolderHandler)
				folder.DELETE("/", handler.DeleteUserFolderHandler)
				folder.GET("/", handler.FindUserFolderHandler)
				folder.PUT("/", handler.UpdateFolderHandler)
				folder.GET("/files", handler.FindFolderFilesHandler)
				folder.PUT("/files", handler.UpdateFileNameHandler)
				folder.PUT("/position", handler.MoveFileToFolderHandler)
				folder.PUT("/files/status", handler.UpdateStatusHandler)
			}
			// 垃圾箱路由
			trash := token.Group("/trash")
			{
				trash.GET("/", handler.FindTrashFilesHandler)
				trash.DELETE("/file", handler.DeleteTrashFileHandler)
				trash.DELETE("/restore", handler.RestoreTrashFileHandler)
			}
			// 文件路由
			file := token.Group("/file")
			{
				file.POST("/upload", handler.UploadHandler)
				file.POST("/second/upload", handler.SecondPassHandler)
				file.GET("/download", handler.DownloadHandler)
				// 分块上传
				file.POST("/mpupload/init", handler.InitialMultipartUploadHandler)
				file.POST("/mpupload/uppart", handler.UploadPartHander)
				file.POST("/mpupload/complete", handler.CompleteUploadHandler)

				//
				file.GET("/downloadurl", handler.DownloadURLHandler)
			}
		}
	}

	return router
}

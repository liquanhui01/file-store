package errors

import (
	"errors"
)

var (
	// 数据表中数据未找到
	ErrNotFound = errors.New("record not found")
)

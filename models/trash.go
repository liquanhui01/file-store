package models

type Trash struct {
	ID        int64  `gorm:"primaryKey;autoIncrement;not null;" json:"id"`
	Name      string `gorm:"not null;" json:"name"`
	UserId    int64  `gorm:"not nill;" json:"user_id"`
	FileSha1  string `gorm:"not null;" json:"file_sha1"`
	FolderId  int64  `gorm:"not null;" json:"folder_id"`
	Type      int64  `gorm:"not null;comment:'0表示文件夹，1表示文件'" json:"type"`
	CreatedAt string
}

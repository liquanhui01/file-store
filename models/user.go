package models

type User struct {
	ID         int64  `gorm:"primary_key;autoIncrement;not null;" json:"id"`
	UserName   string `gorm:"not null;unique;" json:"user_name"`
	Password   string `gorm:"not null;" json:"password"`
	Email      string `gorm:"size:64;" json:"email"`
	Phone      string `gorm:"size:11;" json:"phone"`
	Avatar     string `gorm:"size:1024;" json:"avatar"`
	Profile    string `gorm:"size:1024;" json:"profile"`
	Status     int64  `gorm:"comment:'0表示启用，1表示禁用，2表示锁定，3表示标记删除'" json:"status"`
	LastActive string `json:"last_active"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

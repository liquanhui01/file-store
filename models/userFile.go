package models

type UserFile struct {
	ID         int64  `gorm:"autoIncrement;unique;primaryKey;"`
	UserID     int64  `gorm:"not null;comment:'用户id';uniqueIndex='user_file'" json:"user_id"`
	FileSha1   string `gorm:"not null;uniqueIndex='user_file'" json:"file_sha1"`
	FileSize   string `gorm:"not null;" json:"file_size"`
	FileName   string `gorm:"not null;" json:"file_name"`
	Type       string `gorm:"not null;" json:"type"`
	FolderID   int64  `gorm:"not null;comment:'文件夹id'" json:"folder_id"`
	UploadAt   string `gorm:"not null;" json:"upload_at"`
	LastUpdate string `gorm:"not null;" json:"last_update"`
	Status     int64  `gorm:"not null;default:0;comment:'文件状态(0正常, 1为禁用, 2已删除)'" json:"status"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

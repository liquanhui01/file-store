package models

// UserFolder: 用户存储的文件的文件夹
type UserFolder struct {
	ID         int64  `gorm:"primaryKey;autoIncrement;not null;" json:"id"`
	FolderName string `gorm:"not null;uniqueIndex:user_folder;size:255" json:"folder_name"`
	UserId     int64  `gorm:"not null;uniqueIndex:user_folder" json:"user_id"`
	Status     int64  `gorm:"not null;comment:'0标识可用，1表示禁用，2表示已删除'" json:"status"`
	CreatedAt  string
	UpdatedAt  string
}

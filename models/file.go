package models

// File: 存储的文件
type File struct {
	ID          int64  `gorm:"primaryKey;autoIncrement;not null;" json:"id"`
	FileSha1    string `gorm:"not null;unique;" json:"file_sha1"`
	FileName    string `gorm:"not null;unique;" json:"file_name"`
	FileNumber  int64  `gorm:"not null;default:0" json:"file_number"`
	FileSize    string `gorm:"not null;" json:"file_size"`
	FileAddress string `gorm:"not null;unique;" json:"file_address"`
	Status      int64  `gorm:"not null;default:0;comment:'0表示可用, 1表示已删除'" json:"status"`
	Ext1        int64  `gorm:"default:0" json:"ext1"`
	Ext2        int64  `gorm:"default:0" json:"ext2"`
	CreatedAt   string
	UpdatedAt   string
}

--- user_file表
CREATE TABLE `user_files` (
    `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `user_name` varchar(64) NOT NULL,
    `file_sha1` varchar(64) NOT NULL DEFAULT '' COMMENT '文件的hash',
    `file_size` bigint(20) DEFAULT '0' COMMENT '文件大小',
    `file_name` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
    `upload_at` varchar(100) NOT NULL COMMENT '上传时间',
    `last_update` varchar(100) NOT NULL COMMENT '最后修改时间',
    `status` int(11) NOT NULL DEFAULT '0' COMMENT '文件状态(0正常, 1禁用, 2已删除)',
    `created_at` varchar(100) NOT NULL COMMENT '创建时间',
    `updated_at` varchar(100) NOT NULL COMMENT '更新时间',
    UNIQUE KEY `idx_user_file`(`user_name`, `file_sha1`),
    KEY `idx_status` (`status`)
);

-- user_folder表
CREATE TABLE `user_folders` (
    `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `folder_name` varchar(64) NOT NULL UNIQUE,
    `user_id` int(11) NOT NULL,
    `status` int(11) DEFAULT 0 COMMENT '文件夹状态(0正常, 1禁用, 2删除)',
    `created_at` varchar(100) NOT NULL COMMENT '创建时间',
    `updated_at` varchar(100) NOT NULL COMMENT '更新时间'
);
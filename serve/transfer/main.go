package main

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	conf "file-store/config"
	"file-store/kafka"
	"file-store/repo"
	"file-store/store/oss"
)

func ProcessTransfer(msg []byte) bool {
	// 解析msg
	pubData := kafka.TransferData{}
	err := json.Unmarshal(msg, &pubData)
	if err != nil {
		log.Println(err.Error())
		return false
	}
	fmt.Println("数据为：", pubData)
	// 根据临时存储的路径，创建文件句柄
	file, err := os.Open(pubData.CurAddress)
	if err != nil {
		log.Println(err.Error())
		return false
	}
	// 通过文件句柄将文件内容读取出来并上传到oss
	fmt.Println("进入到oss上传程序")
	err = oss.Bucket().PutObject(pubData.DestAddress, bufio.NewReader(file))
	if err != nil {
		log.Println(err.Error())
		return false
	}
	// 更新文件的存储路径到文件表
	ctx := context.Background()
	err = repo.UpdateFile(ctx, pubData.FileSha1, pubData.DestAddress)
	if err != nil {
		log.Println(err.Error())
		return false
	}
	return true
}

func main() {
	if !conf.Rq.AsyncTransferEnable {
		log.Println("异步转移文件功能目前被禁用，请检查相关配置")
		return
	}
	log.Println("文件转移服务启动中，开始监听转移任务队列...")
	// mq.StartConsume(
	// 	"uploadserver.trans.oss",
	// 	"tansfer_oss",
	// 	ProcessTransfer,
	// )
}

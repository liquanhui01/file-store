package main

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"

	conf "file-store/config"
	"file-store/kafka"
	"file-store/models"
	"file-store/route"
)

func main() {
	configPath := "/Users/apple/workplace/file-store/config"
	// 配置初始化
	err := conf.InitConfig(configPath, "config", "yaml")
	if err != nil {
		fmt.Printf("Failed to init config, err is %s\n", err)
		panic(err)
	}
	// 获取全局配置
	conf := conf.GetConfig()
	// 数据库操作
	models.MysqlInit(conf.Mysql.Drive, conf.Mysql.Address)

	// 设置gin模式，必须设置在路由前边
	gin.SetMode(gin.DebugMode)

	// gin框架路由设置
	router := route.Router()

	go func() {
		err = router.Run(conf.Server.Path)
		if err != nil {
			log.Fatalf(err.Error())
		}
	}()
	go func() {
		kafka.InitKafka()
		kafka.InitConsumer()
	}()
	select {}
}

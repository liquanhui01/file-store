package config

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	File   *File   `yaml:"file"`
	Mysql  *Mysql  `yaml:"mysql"`
	Token  *Token  `yaml:"token"`
	Server *Server `yaml:"server"`
	Log    *Log    `yaml:"log"`
	Redis  *Redis  `yaml:"redis"`
	OSS    *OSS    `yaml:"oss"`
	// Rabbitmq *Rabbitmq `yaml:"rabbitmq"`
}

type File struct {
	Path string `yaml:"path"`
}

type Mysql struct {
	Drive   string `yaml:"drive"`
	Address string `yaml:"address"`
}

type Token struct {
	Salt        string `yaml:"salt"`
	Issue       string `yaml:"issue"`
	ExpiredTime int64  `yaml:"expired_time"` // 有效时间
}

type Server struct {
	Path string `yaml:"path"`
}

type Log struct {
	FilePath     string `yaml:"file_path"`
	FileName     string `yaml:"file_name"`
	MaxAge       int64  `yaml:"max_age"`
	RetationTime int64  `yaml:"retation_time"`
}

type Redis struct {
	Host string `yaml:"host"`
	Pass string `yaml:"pass"`
}

type OSS struct {
	// oss bucket名
	OSSBucket string `yaml:"oss_bucket"`
	// endpoint
	OSSEndpoint string `yaml:"oss_end_point"`
	// oss访问密钥
	OSSAccesskeyID string `yaml:"oss_access_key_ID"`
	// oss访问key screct
	OSSAccessKeySecret string `yaml:"oss_access_key_secret"`
}

// type Rabbitmq struct {
// 	AsyncTransferEnable  bool   `yaml:"async_transfer_enable"`
// 	RabbitURL            string `yaml:"rabbit_url"`
// 	TransExchangeName    string `yaml:"trans_exchange_name"`
// 	TransOSSQueueName    string `yaml:"trans_oss_queue_name"`
// 	TransOSSErrQueueName string `yaml:"trans_oss_err_queue_name"`
// 	TransOSSRoutingKey   string `yaml:"trans_oss_routing_key"`
// }

// 全局配置
var GlobalConfig = new(Config)

// InitConfig 读取yaml配置文件
func InitConfig(configPath, configName, configType string) error {
	viper.SetConfigName(configName) // 配置文件名
	viper.SetConfigType(configType) // 配置文件类型，例如:toml、yaml等
	viper.AddConfigPath(configPath) // 查找配置文件所在的路径，多次调用可以添加多个配置文件搜索的目录
	// 读取配置文件配置，并处理错误
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			return err
		}
	}
	// 监控配置文件变化
	viper.WatchConfig()
	viper.Unmarshal(GlobalConfig)
	if err := validateConfig(GlobalConfig); err != nil {
		return err
	}
	return nil
}

// 获取全局配置
func GetConfig() *Config {
	return GlobalConfig
}

// validateConfig：校验配置信息
func validateConfig(conf *Config) error {
	var (
		file    = conf.File.Path
		drive   = conf.Mysql.Drive
		address = conf.Mysql.Address
		salt    = conf.Token.Salt
		issue   = conf.Token.Issue
	)
	if file == "" {
		return fmt.Errorf("invalid file path: %s\n", file)
	}
	if drive == "" {
		return fmt.Errorf("invalid drive: %s\n", drive)
	}
	if address == "" {
		return fmt.Errorf("invalid address: %s\n", address)
	}
	if salt == "" {
		return fmt.Errorf("invalid salt: %s\n", salt)
	}
	if issue == "" {
		return fmt.Errorf("invalid issue: %s\n", issue)
	}
	return nil
}

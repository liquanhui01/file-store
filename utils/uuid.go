package utils

import (
	"errors"
	"strconv"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
)

// GenerateFileName: 根据后缀、当前时间戳和uuid生成新的文件名，
// jpeg、jpg转换成png格式，所有的图片全部以png格式存储
// 使用uuid来实现文件名的唯一性
func GenerateFileName(fileName string) (string, error) {
	// 获取后缀
	suffix := strings.Split(fileName, ".")[1]
	switch suffix {
	case "":
		return "", errors.New("file type incorrect")
	case "jpeg", "jpg", "png":
		suffix = "png"
	}
	// uuid和时间戳生成新的文件名
	fileName = uuid.NewV4().String() + strconv.FormatInt(time.Now().UnixNano(), 10)
	return fileName + "." + suffix, nil
}

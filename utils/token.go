package utils

import (
	"crypto/md5"
	"encoding/hex"
	conf "file-store/config"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Claims struct {
	Flag string
	jwt.StandardClaims
}

// GenerateToken：生成jwt
func GenerateToken(userID int64) (string, error) {
	tokenConfig := conf.GlobalConfig.Token
	ex := tokenConfig.ExpiredTime
	flag := GenerateUserFlag(userID)
	expiredTime := time.Now().Add(time.Duration(ex) * time.Second)
	claims := &Claims{
		Flag: flag,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiredTime.Unix(), // 过期时间
			IssuedAt:  time.Now().Unix(),  // 颁发时间
			Id:        flag,               // 编号
			Issuer:    tokenConfig.Issue,  // 颁发者
			NotBefore: time.Now().Unix(),  // 生效时间
			Subject:   "user token",       // token主题
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(tokenConfig.Salt))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// ParseToken: 解析token
func ParseToken(token string) (*Claims, error) {
	tokenConfig := conf.GlobalConfig.Token
	tokenClaims, err := jwt.ParseWithClaims(token, &Claims{}, func(token *jwt.Token) (
		interface{}, error) {
		return []byte(tokenConfig.Salt), nil
	})
	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}
	return nil, err
}

// GenerateUserFlag: 根据id使用md5生成用户唯一标识
func GenerateUserFlag(userID int64) string {
	md5 := md5.New()
	md5.Write([]byte(strconv.FormatInt(userID, 10)))
	return hex.EncodeToString(md5.Sum([]byte("")))
}

package utils

import (
	conf "file-store/config"
	md "file-store/models"
	"strconv"
	"strings"
	"time"
)

// FormatTime: 时间格式化
func FormatTime(time time.Time) string {
	return time.Format("2006-01-02 15:04:05")
}

// FormatFilePath: 文件地址拼接
func FormatFilePath(fileName string) string {
	var builder strings.Builder
	builder.WriteString(conf.GlobalConfig.File.Path)
	builder.WriteString(fileName)
	return builder.String()
}

// FormatFileMate: 文件元数据
func FormatFileMate(fileName, fileSha1, ossPath, fileSize string, status int64) (
	fileMeta *md.File) {
	fm := &md.File{
		FileName: fileName,
		FileSha1: fileSha1,
		// FileAddress: FormatFilePath(fileName),
		FileAddress: ossPath,
		FileSize:    fileSize,
		Status:      status,
		CreatedAt:   FormatTime(time.Now()),
		UpdatedAt:   FormatTime(time.Now()),
	}
	return fm
}

// StringTransToInt64: 字符串转换成int64
func StringTransToInt64(par string) int64 {
	res, _ := strconv.ParseInt(par, 10, 64)
	return res
}

package utils

import (
	"encoding/json"
	"log"
)

// 响应的通用数据结构
type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// Newresponse: 生成Response对象
func Newresponse(code int, msg string, data interface{}) *Response {
	return &Response{
		Code: code,
		Msg:  msg,
		Data: data,
	}
}

// JSONString: 对象转json格式为string
func (resp *Response) JSONString() string {
	r, err := json.Marshal(resp)
	if err != nil {
		log.Println(err)
	}
	return string(r)
}

// JSONBytes: 对象转json格式为二进制数组
func (resp *Response) JSONBytes() []byte {
	r, err := json.Marshal(resp)
	if err != nil {
		log.Println(err)
	}
	return r
}

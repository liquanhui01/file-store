package utils

import (
	conf "file-store/config"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"strconv"
	"strings"
)

// CheckFileExists: 判断文件是否已存在，如果存在了则根据相同文件的数量进行文件重命名
func CheckFileExists(fileName, filePath string, number int64) (isExist bool, name string) {
	totalPath := conf.GlobalConfig.File.Path + fileName
	if totalPath != filePath {
		return false, fileName
	}
	num := strconv.FormatInt(number, 10)
	return true, fileName + "(" + num + ")"
}

// UploadFile: 上传文件
func UploadFile(filePath string, file multipart.File) (f *os.File, fileSha1, fileSize string, err error) {
	newFile, err := os.Create(filePath)
	if err != nil {
		return nil, "", "", err
	}
	defer newFile.Close()
	fz, err := io.Copy(newFile, file)
	if err != nil {
		return nil, "", "", err
	}
	newFile.Seek(0, 0)
	// 生成fileSha1
	fileSha1 = FileSha1(newFile)
	// 生成带单位的文件大小
	fileSize = computeSize(fz)
	return newFile, fileSha1, fileSize, nil
}

// CheckFileType: 判断文件类型
func CheckFileType(fileName string) string {
	strArr := strings.Split(fileName, ".")
	switch strArr[len(strArr)-1] {
	case "jpeg", "jpg", "png", "gif", "tif":
		return "图片"
	case "docx", "xlsx", "txt", "pptx", "pdf":
		return "文本文件"
	case "mp3", "mp4", "mpg", "mpeg", "avi":
		return "影音"
	case "zip", "taz", "exe", "rar", "bzip2", "zipx":
		return "压缩文件"
	default:
		return ""
	}
}

// ComputeSize: 计算并返回带有单位的文件大小
func computeSize(fileSize int64) string {
	var (
		fz  float64
		suf string
	)
	switch {
	case fileSize < 1024:
		fz = float64(fileSize)
		suf = ""
	case fileSize < 1048576 && fileSize > 1024:
		fz = float64(fileSize) / (float64(1024))
		suf = "K"
	case fileSize < 1073741824 && fileSize > 1048576:
		fmt.Println(fileSize)
		fz = float64(fileSize) / float64(1048576)
		suf = "M"
	case fileSize < 1099511627776 && fileSize > 1073741824:
		fz = float64(fileSize) / float64(1073741824)
		suf = "G"
	default:
		fz, suf = 0, ""
		return "文件超出上传的大小"
	}
	return spliceSizeAndSuf(fz, suf)
}

// SpliceString: 字符串拼接
func SpliceString(str ...string) string {
	var build strings.Builder
	for _, arg := range str {
		build.WriteString(arg)
	}
	return build.String()
}

// SpliceSizeAndSuf: 拼接size和单位
func spliceSizeAndSuf(fileSize float64, suf string) string {
	fzStr := strconv.FormatFloat(fileSize, 'f', 2, 64)
	return SpliceString(fzStr, suf)
}

// RenameFile: 文件重命名
func RenameFile(newFileName, originFileName string) string {
	fileArray := strings.Split(originFileName, ".")
	suffix := fileArray[len(fileArray)-1]
	return SpliceString(newFileName, ".", suffix)
}

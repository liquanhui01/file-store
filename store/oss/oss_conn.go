package oss

import (
	conf "file-store/config"
	"fmt"
	"os"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

var (
	ossCli *oss.Client
)

// OssClient: 创建oss client存储对象
func OssClient() *oss.Client {
	cf := conf.GlobalConfig.OSS
	if ossCli != nil {
		return ossCli
	}
	ossCli, err := oss.New(
		cf.OSSEndpoint,
		cf.OSSAccesskeyID,
		cf.OSSAccessKeySecret,
	)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	return ossCli
}

// Bucket: 获取bucket存储空间
func Bucket() *oss.Bucket {
	cf := conf.GlobalConfig.OSS
	cli := OssClient()
	if cli.Bucket == nil {
		return nil
	}
	bucket, err := cli.Bucket(cf.OSSBucket)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	return bucket
}

// DownloadURL: 临时授权下载
func DownloadURL(objName string) string {
	signedURL, err := Bucket().SignURL(objName, oss.HTTPGet, 3600)
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	return signedURL
}

// BuildLifeCycleRule: 针对bucket设置生命周期规则
func BuildLifeCycleRule(bucketName string) {
	// 表示前缀为test的对象(文件)距最后修改时间1天后过期
	ruleTest1 := oss.BuildLifecycleRuleByDays("rule1", "test/", true, 1)
	rules := []oss.LifecycleRule{ruleTest1}
	err := OssClient().SetBucketLifecycle(bucketName, rules)
	if err != nil {
		fmt.Printf("Failed to set oss lifecycle, err is: %s", err.Error())
		os.Exit(-1)
	}
}

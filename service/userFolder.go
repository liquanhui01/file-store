package service

import (
	"context"
	md "file-store/models"
	"file-store/repo"
)

// CreateUserFolder: 创建用户文件夹
func CreateUserFolder(ctx context.Context, userId int64, folderName string) (*md.UserFolder, error) {
	return repo.CreateUserFolder(ctx, userId, folderName)
}

// DeleteUserFolderById: 根据userId删除用户文件夹
func DeleteUserFolderById(ctx context.Context, userId, folderId, status int64) error {
	return repo.UpdateUserFolderStatus(ctx, userId, folderId, status)
}

// DeleteUserFolder: 彻底删除文件夹
func DeleteUserFolder(ctx context.Context, folderId int64) error {
	return repo.DeleteUserFolder(ctx, folderId)
}

// FindFolderByUserId: 根据user_id查询用户所有的文件夹
func FindFolderByUserId(ctx context.Context, userId int64) ([]*md.UserFolder, error) {
	return repo.FindFolderByUserId(ctx, userId)
}

// UpdateFolderByUserId: 根据user_id更新文件夹名称
func UpdateFolderByUserId(ctx context.Context, userId, folderId int64, folderName string) (string, error) {
	return repo.UpdateFolderByUserId(ctx, userId, folderId, folderName)
}

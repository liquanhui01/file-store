package service

import (
	"context"
	md "file-store/models"
	repo "file-store/repo"
)

// CreateUserFile: 创建用户文件记录
func CreateUserFile(ctx context.Context, userId int64, fileSha1, fileName, fileType, fileSize string,
	folderId int64) (id int64, err error) {
	return repo.CreateUserFile(ctx, userId, fileSha1, fileName, fileType, fileSize, folderId)
}

// FindUserFileByNameAndSha1: 根据file_sha1和user_id查询数据
func FindUserFileByUserAndSha1(ctx context.Context, userId int64, fileSha1 string) (
	*md.UserFile, error) {
	return repo.FindUserFileByUserAndSha1(ctx, userId, fileSha1)
}

// 根据user_id和folder_id查找指定文件夹下的所有文件信息
func FindFilesByUserAndFolder(ctx context.Context, userId, folderId int64) ([]*md.UserFile,
	error) {
	return repo.FindFilesByUserAndFolder(ctx, userId, folderId)
}

// UpdarteStatusByFileSha1AndUserId：根据userId和fileSha1修改文件的状态
func UpdateStatusByFileSha1AndUserId(ctx context.Context, name, fileSha1 string, userId,
	fileType, folder_id, status int64) error {
	return repo.UpdateStatusByFileSha1AndUserId(ctx, name, fileSha1, userId, fileType, folder_id, status)
}

// DeleteUserFileByFileSha1: 根据file_sha1删除指定的用户文件数据
func DeleteUserFileByFileSha1(ctx context.Context, fileSha1 string, userId int64) error {
	return repo.DeleteUserFileByFileSha1(ctx, fileSha1, userId)
}

// UpdateFileName: 文件重命名
func UpdateFileName(ctx context.Context, fileSha1, fileName string) (string, string, error) {
	return repo.UpdateFileName(ctx, fileSha1, fileName)
}

// MoveFileToFolder：移动文件到指定的文件夹
func MoveFileToFolder(ctx context.Context, fileSha1 string, folderId int64) error {
	return repo.MoveFileToFolder(ctx, fileSha1, folderId)
}

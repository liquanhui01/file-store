package service

import (
	"context"
	md "file-store/models"
	repo "file-store/repo"
	utils "file-store/utils"
	"fmt"
)

// CreateUser: 注册用户
func CreateUser(ctx context.Context, userName, password, phone string) (id int64, err error) {
	hashedPassword, err := utils.GenerateHashPassword(password)
	if err != nil {
		fmt.Printf("Failed to generate hashed password, err is %s\n", err)
	}
	return repo.CreateUser(ctx, userName, string(hashedPassword), phone)
}

// UserLogin:用户登陆
func UserLogin(ctx context.Context, userName, password string) (
	userInfo *md.User, token string, err error) {
	user, err := repo.FindUserByUserName(ctx, userName)
	if err != nil {
		return nil, "", err
	}
	err = utils.ValidatePassword(password, user.Password)
	if err != nil {
		return nil, "", err
	}
	// 生成token
	token, err = utils.GenerateToken(user.ID)
	if err != nil {
		return nil, "", err
	}
	err = repo.UpdateLastActiveById(ctx, user.ID)
	if err != nil {
		return nil, "", err
	}
	return user, token, nil
}

// FindUserByid: 根据id查找用户数据
func FindUserById(ctx context.Context, id int64) (*md.User, error) {
	user, err := repo.FindUserById(ctx, id)
	if err != nil {
		return nil, err
	}
	partUser := &md.User{
		ID:         user.ID,
		UserName:   user.UserName,
		Email:      user.Email,
		Phone:      user.Phone,
		Profile:    user.Profile,
		Status:     user.Status,
		LastActive: user.LastActive,
		CreatedAt:  user.CreatedAt,
	}
	return partUser, nil
}

// DeleteUserById: 根据id删除用户信息
func DeleteUserById(ctx context.Context, id int64) error {
	return repo.DeleteUserById(ctx, id)
}

// UpdateUserPasswordById: 根据id更新用户密码
func UpdateUserPasswordById(ctx context.Context, newPassword string, id int64) error {
	return repo.UpdateUserPasswordById(ctx, newPassword, id)
}

// UpdateUser: 更新用户信息
func UpdateUser(ctx context.Context, id int64, userName, profile, phone string) error {
	return repo.UpdateUser(ctx, id, userName, profile, phone)
}

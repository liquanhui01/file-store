package service

import (
	"context"
	md "file-store/models"
	"file-store/repo"
)

// CreateTrash: 创建垃圾文件和文件夹
func CreateTrash(ctx context.Context, name, fileSha1 string, fileType, folderId, userId int64) error {
	return repo.CreateTrash(ctx, name, fileSha1, fileType, folderId, userId)
}

// DeleteFileById: 根据userId和id删除指定的垃圾文件
func DeleteTrashFileById(ctx context.Context, userId, fileType, folderId, id int64, fileSha1 string) error {
	return repo.DeleteTrashFileById(ctx, userId, fileType, folderId, id, fileSha1)
}

// DeleteFindById: 清空指定用户垃圾箱内的所有垃圾文件和文件夹
func DeleteAllById(ctx context.Context, userId int64) error {
	return repo.DeleteAllById(ctx, userId)
}

// FindTrashFilesById: 根据用户id查找所有的垃圾文件或文件夹
func FindTrashFilesById(ctx context.Context, userId int64) ([]*md.Trash, error) {
	return repo.FindTrashFilesById(ctx, userId)
}

// RestoreTrashFile: 还原垃圾箱中的文件或文件夹
func RestoreTrashFile(ctx context.Context, userId, fileType, folderId, id, status int64, fileSha1 string) error {
	return repo.RestoreTrashFile(ctx, userId, fileType, folderId, id, status, fileSha1)
}

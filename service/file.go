package service

import (
	"context"
	md "file-store/models"
	repo "file-store/repo"
)

// CreateFile: 创建文件数据
func CreateFile(ctx context.Context, file *md.File) (id int64, err error) {
	return repo.CreateFile(ctx, file)
}

// UpdateFileNameByFilehash: 根据fileSha1和fieName修改文件元数据(重命名)
func UpdateFileNameByFilehash(ctx context.Context, fileSha1, fileName string) error {
	return repo.UpdateFileNameByFilehash(ctx, fileSha1, fileName)
}

// FindFileMetaByFilehash: 根据filesha1查找文件的元数据
func FindFileMetaByFileSha1(ctx context.Context, fileSha1 string) (
	file *md.File, err error) {
	return repo.FindFileByFileSha1(ctx, fileSha1)
}

// FindFileQueryByLimit: 查找最新几条文件的数据
func FindFileQueryByLimit(ctx context.Context, limit int64) ([]*md.File, error) {
	return repo.FindFileQueryByLimit(ctx, limit)
}

// DeleteFileByFilehash: 根据file_sha1删除指定文件数据
func DeleteFileByFilehash(ctx context.Context, fileSha1 string) error {
	return repo.DeleteFileByFilehash(ctx, fileSha1)
}

// FindFileByName: 根据名字查找文件信息
func FindFileByName(ctx context.Context, fileName string) (*md.File, error) {
	return repo.FindFileByName(ctx, fileName)
}
